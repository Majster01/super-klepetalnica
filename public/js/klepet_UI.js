/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var krc = sporocilo.indexOf("&#9756;") > -1;
  if (krc) {
    sporocilo = sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;");
    return divElementHtmlTekst(sporocilo);
  } 
  var jeSlika = (sporocilo.indexOf("http") && (sporocilo.indexOf(".jpg") || sporocilo.indexOf(".png") || sporocilo.indexOf(".gif"))) > -1;
  if (jeSlika) {
    if (sporocilo.indexOf(".jpg.")) {
      sporocilo = sporocilo.split(".jpg.").join('.jpg .');
    } if (sporocilo.indexOf(".png.")) {
      sporocilo = sporocilo.split(".png.").join('.png .');
    } if (sporocilo.indexOf(".gif.")) {
      sporocilo = sporocilo.split(".gif.").join('.gif .');
    }
    var besede = sporocilo.split(' ');
    var slike = [];

    for (var i in besede) {
      //console.log(besede[i].match(/https?:\/\/.+\.?:(jpg|gif|png)/));
      if(besede[i].match(/https?:\/\/.+\.(jpg|gif|png)/)) {
        slike.push(besede[i]);
        console.log('"' + besede[i] + '"');
      }
    }
    
    for (var i in slike) {
      sporocilo += ('<br/><img style="width: 200px; padding-left: 20px" src="' + slike[i] + '">');
    }
    
    if (sporocilo.indexOf(".jpg .")) {
      sporocilo = sporocilo.split(".jpg .").join('.jpg.');
    } if (sporocilo.indexOf(".png .")) {
      sporocilo = sporocilo.split(".png .").join('.png.');
    } if (sporocilo.indexOf(".gif .")) {
      sporocilo = sporocilo.split(".gif .").join('.gif.');
    }
    
    return divElementHtmlTekst(sporocilo);
  }
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  console.log(sporocilo);
  if (sporocilo.indexOf("(zasebno za ") > -1) {
    var jeSlika = (sporocilo.indexOf("http") && (sporocilo.indexOf(".jpg") || sporocilo.indexOf(".png") || sporocilo.indexOf(".gif"))) > -1;
    if (jeSlika) {
      if (sporocilo.indexOf(".jpg.")) {
        sporocilo = sporocilo.split(".jpg.").join('.jpg .');
      } if (sporocilo.indexOf(".png.")) {
        sporocilo = sporocilo.split(".png.").join('.png .');
      } if (sporocilo.indexOf(".gif.")) {
        sporocilo = sporocilo.split(".gif.").join('.gif .');
      }
      var besede = sporocilo.split(' ');
      var slike = [];
  
      for (var i in besede) {
        //console.log(besede[i].match(/https?:\/\/.+\.?:(jpg|gif|png)/));
        if(besede[i].match(/https?:\/\/.+\.(jpg|gif|png)/)) {
          slike.push(besede[i]);
          console.log('"' + besede[i] + '"');
        }
      }
      
      for (var i in slike) {
        sporocilo += ('<br/><img style="width: 200px; padding-left: 20px" src="' + slike[i] + '">');
      }
      
      if (sporocilo.indexOf(".jpg .")) {
        sporocilo = sporocilo.split(".jpg .").join('.jpg.');
      } if (sporocilo.indexOf(".png .")) {
        sporocilo = sporocilo.split(".png .").join('.png.');
      } if (sporocilo.indexOf(".gif .")) {
        sporocilo = sporocilo.split(".gif .").join('.gif.');
      }
    }  
  }
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var pUporabniki = [];
var vzdevki = [];

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  socket.on('sprememba', function(rezultat) {
    for (var i in pUporabniki) {
      for (var j in rezultat) {
        if (pUporabniki[i] == rezultat[j].staro) {
          pUporabniki[i] = rezultat[j].novo;
        }
      }
    }
  });
  
  socket.on('preimenujOdgovor', function(rezultat) {
    var novUporabnik = true;
    for (var i in pUporabniki) {
      if (pUporabniki[i] == rezultat.sporocilo.uporabnik) {
        vzdevki[i] = rezultat.sporocilo.vzdevek;
        novUporabnik = false;
      }
    }
    if (novUporabnik && rezultat!=null) {
      pUporabniki.push(rezultat.sporocilo.uporabnik);
      vzdevki.push(rezultat.sporocilo.vzdevek);
    }
  });
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    //console.log(rezultat.tabelaVzdevkov);
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    //console.log(pUporabniki);
    var text = sporocilo.besedilo.split(" ");
    //console.log(text[0]);
    for (var i in pUporabniki) {
      var jePreimenovan = text[0].indexOf(pUporabniki[i]) > -1;
      if (jePreimenovan) {
        console.log("if stavek");
        text[0] = text[0].split(pUporabniki[i]).join(vzdevki[i]+" ("+pUporabniki[i]+")");
      }
    }
    text = text.join(" ");
    var novElement = divElementEnostavniTekst(text);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    for (var i=0; i < uporabniki.length; i++) {
      var preimenovanje = false;
      for (var j in pUporabniki) {
        if(uporabniki[i] == pUporabniki[j]) {
          preimenovanje = true;
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(vzdevki[j]+" ("+uporabniki[i]+")"));
        }
      }
      if (!preimenovanje) {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
    }
    
     // Klik na ime uporabnika v seznamu uporabnikov zazene krcanje
    $('#seznam-uporabnikov div').click(function() {
      //console.log($(this).text());
      var ime = ($(this).text()).split(' ');
      var prejemnik = $(this).text();
      if (ime.length > 1) {
        prejemnik = ime[1];
        prejemnik = prejemnik.split('(').join('').
                              split(')').join('');
        console.log(prejemnik);
      }
      klepetApp.procesirajUkaz('/zasebno "' + prejemnik + '" "&#9756;"');
      var novElement = divElementEnostavniTekst('(zasebno za ' + $(this).text() + '): &#9756;');
      $('#sporocila').append(novElement);
      $('#poslji-sporocilo').focus();
    });
  });
  
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
    socket.emit('sprememba');
    socket.emit('vzdevki', {
      vzdevki: vzdevki,
      pUporabniki: pUporabniki
    });
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
